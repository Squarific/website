---
    title: "Hackerspace Brussels (HSBXL)"
    site: "https://hsbxl.be/"
    city: "Brussels"
    tel: "003228804004"
    location: "Rue des Goujons 152, 1070 Anderlecht, Brussels"
    contact: "contact@hsbxl.be"
    irc: "#hsbxl on irc.freenode.net"
    matrix: #hsbxl:hackerspace.be
    openday: Tuesday evening
    aliases: [spaces/HSBXL.html] #redirect from old site uri
---



Hacker Space Brussels (HSBXL) is a space, dedicated to various aspects of constructive & creative hacking. The space is about 120 square meters, there is a little electronics lab with over 9000 components, a library, and lots of tools. You're always welcome to follow one of the workshops or come to the weekly Tuesday meetings, hack nights or other get-together events.

* Location: [Rue des Goujons 152, 1070 Anderlecht, Brussels](https://goo.gl/maps/BUABaDL9Trv)
* Site: [hsbxl.be](https://hsbxl.be)
* Chat (matrix): [#hsbxl:hackerspaces.be](https://ptt.hackerspaces.be/#/room/#hsbxl:hackerspaces.be)